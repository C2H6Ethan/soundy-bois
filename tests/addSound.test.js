import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';

jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);

addSound = async() => {
    // function to add new sound
    var name =  'soundName'
    name = name.substring(0, 12);
    name = name.toLowerCase()

    var sounds = []
    var sound = {id: sounds.length,name: name, sound: "testUri"}
    sounds.push(sound);

    var newSounds = {sounds : sounds};
    // save new sound to device storage
    await mockAsyncStorage.setItem("sounds", JSON.stringify(newSounds));

}

addSound()

test('add sound', async() => {
    var sounds = await mockAsyncStorage.getItem("sounds");
    sounds = JSON.parse(sounds);
    sounds = sounds['sounds'];

    expect(sounds.length).toBe(1);
});