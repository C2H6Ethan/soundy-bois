import { StatusBar } from 'expo-status-bar';
import AsyncStorage  from "@react-native-async-storage/async-storage";
import React, { Component } from 'react';
 
import { StyleSheet, View, Image, TouchableOpacity, Alert, Text, ScrollView,SafeAreaView, Modal, KeyboardAvoidingView, TextInput, Pressable } from 'react-native';
import SoundButton from './components/SoundButton';
import * as DocumentPicker from 'expo-document-picker';

export default class App extends Component {

  constructor (props){
    super(props);

    this.state = {
      modalVisible: false,
      modalInputText: '',
      sounds: [],
      audioFile: null,
      selectAudioButtonText: 'Select Audio'
    };
  }


  // function runs when app is compiled
  componentDidMount = async() => {
    // load all sound
    var sounds = await AsyncStorage.getItem("sounds");
    if(sounds != null){
      sounds = JSON.parse(sounds);
      sounds = sounds['sounds'];
      this.setState({sounds: sounds})
    }
  }

  pickDocument = async () => {
    // select audio file from device
    let options = {
     type:'audio/*'
    }
    let result = await DocumentPicker.getDocumentAsync(options);
    
    if(result.uri != null){
      this.setState({audioFile: result, selectAudioButtonText: result.name})
    }
    
  }

  setModalVisible = (visible) => {
    // show or hide add sound modal 
    this.setState({ modalVisible: visible, audioFile: null, selectAudioButtonText: 'Select Audio', modalInputText: '' });

  }

  addSound = async() => {
    // function to add new sound
    if(this.state.audioFile == null){Alert.alert("No Audio File Selected")}
    else{
      var name = this.state.modalInputText
      if(name == ''){name = this.state.audioFile.name; name = name.substring(0, 12);}
      name = name.toLowerCase()

      var sounds = this.state.sounds
      var sound = {id: sounds.length,name: name, sound: this.state.audioFile.uri}
      sounds.push(sound);

      this.setState({sounds: sounds})
      // close modal
      this.setModalVisible(!this.state.modalVisible);
      var newSounds = {sounds : sounds};
      // save new sound to device storage
      await AsyncStorage.setItem("sounds", JSON.stringify(newSounds));
    }
    
  }

  soundsList() {
    // list sound buttons in gui
    if(this.state.sounds.length > 0){
        return this.state.sounds.map((data, index) => {
            return (
              <TouchableOpacity activeOpacity={1} onPress={() => console.warn("name pressed")}>
                <SoundButton id={data['id']} name={data['name']} sound={data['sound']}/>
              </TouchableOpacity>
                
            )
          })
    }
  }

  // view
  render(){
    const { modalVisible } = this.state;
    return (
      <SafeAreaView style={styles.container}>
          {/* add sound button modal */}
         <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                  this.setModalVisible(!modalVisible);
              }}
              >
              <TouchableOpacity activeOpacity={1} onPress={() => this.setModalVisible(!modalVisible)} style={{flex: 1,justifyContent: 'center',alignItems: 'center'}}>
              <TouchableOpacity activeOpacity={1} style={{width: '60%', height: '40%'}}>
                <View style={styles.modalView}>
                <TextInput
                    placeholder="sound name"
                    placeholderTextColor="lightgray" 
                    onChangeText={ text => this.setState({modalInputText: text})}
                    style={styles.modalInput}
                    maxLength={12}
                />
                <Pressable
                  onPress={() => this.pickDocument()}
                  style={styles.modalButton}
                >
                  <Text>{this.state.selectAudioButtonText}</Text>
                </Pressable>
                <Pressable
                    onPress={() => this.addSound()}
                    style={styles.modalButton}
                >
                    <Text>Add</Text>
                </Pressable>
                </View>
              </TouchableOpacity>
              </TouchableOpacity>
          </Modal>

        {/* list of sounds inside a scrollable view */}
        <ScrollView>
            <View style={styles.buttonsContainer}>
                {this.soundsList()}
            </View>
        </ScrollView>

        {/* add sound button which opens a modal */}
        <TouchableOpacity activeOpacity={0.5} onPress={() => this.setModalVisible(!modalVisible)} style={styles.TouchableOpacityStyle} >
          <Image source={require('./assets/addButton.png')}
          
          style={styles.FloatingButtonStyle} />
        </TouchableOpacity>

        <StatusBar style="auto" />
      </SafeAreaView>
    );
  } 
}

// gui styling
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#303030',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonsContainer: {
    alignItems: 'center',
    paddingHorizontal: 50,
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingBottom: 80,
  },
  TouchableOpacityStyle:{
 
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
  },
 
  FloatingButtonStyle: {
 
    resizeMode: 'contain',
    width: 75,
    height: 75,
  },
  modalView: {
    flex: 1,
    backgroundColor: '#303030',
    padding: 35,
    alignItems: 'center',
    justifyContent: 'space-around',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 4,
    borderRadius: 80,
    elevation: 5,
  },
  modalInput: {
    textAlign: 'center',
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: '#303030',
    borderRadius: 60,
    borderColor: 'white',
    borderWidth: 1,
    minWidth: '50%',
    color: 'white',
  },
  modalButton: {
    backgroundColor: 'purple',
    borderRadius: 30,
    padding: 10,
    elevation: 2,
  },
});
