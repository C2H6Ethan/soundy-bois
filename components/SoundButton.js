import React, { Component } from 'react';
import AsyncStorage  from "@react-native-async-storage/async-storage";
import { View, Text, StyleSheet, TouchableOpacity, Image, Alert } from 'react-native';
import { Audio } from 'expo-av';

//sound button component
export default class SoundButton extends Component {

    constructor(props) {
        super(props);

        this.state = {
            source: require('../assets/soundButton.png'),
            id: props['id'],
            name: props['name'],
            sound: props['sound']
        };
    }


    buttonDown = () => {
        // change button image to pressed
        this.setState({ source: require('../assets/soundButtonClicked.png') })
    }

    buttonUp = () => {
        // change button image to not pressed
        this.setState({ source: require('../assets/soundButton.png') })
        this.playSound();
    }

    playSound = async () => {
        // play sound

        const { sound } = await Audio.Sound.createAsync(
            { uri: this.state.sound }
        );

        console.log('Playing Sound');
        await sound.playAsync();
    }

    // display button in gui
    render() {
        return (
            <View style={styles.item}>
                {/* sound button */}
                <TouchableOpacity activeOpacity={1} onPressIn={() => this.buttonDown()} onPressOut={() => this.buttonUp()} >
                    <Image
                        style={styles.buttonImage}
                        source={this.state.source}
                    />
                </TouchableOpacity>
                {/* sound name */}
                <Text style={styles.text}>{this.state.name}</Text>
            </View>
        )
    }

}

// button styling
const styles = StyleSheet.create({
    item: {

        alignContent: 'center',
        justifyContent: 'center',
        marginBottom: 20,

    },
    buttonImage: {
        width: 75,
        height: 75,
        marginHorizontal: 10,
    },
    text: {
        alignSelf: 'center',
        color: 'white'
    }
});